NC Code Generation
====================

	
Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:

Introduction
-------------
The NC Code Generation toolchain is a collection of programs for automatically generating NC code using MasterCAM.  The toolchain contains components for recognizing geometic features in a CAD model, determining appropriate feeds and speeds for the given materials and tools, assessing the feasibility of fixturing the raw stock, and generating NC code automatically within MasterCAM.

Requirements
------------
#. Visual Studio 2010 or later
#. MasterCAM

General Design
---------------
The toolchain is a collection of command line executables which can be used
individually or chained together to produce the desired result. We modeled the
design on a UNIX design philosphy:

Rule of Modularity: Developers should build a program out of simple parts
connected by well defined interfaces, so problems are local, and parts of the
program can be replaced in future versions to support new features.

Stepping Stone
----------------
In addition to the tool exectuables is a Python helper script called
StepStone.py or Stepping Stone. SteppingStone uses the toolchain executables to
create NC code from a STEP file. Like the other programs StepStone.py is
designed to be run on the command line and accepts a plethora of options to
control its behavior.

Stepping Stone Configuration
-------------------------------
StepStone.py should mostly work out of the box but may require configuration if
you're doing something out of the ordinary.

We provided a default options file called StepStone.opt. This file contains a
list of command line arguments which will be passed to the StepStone.py script.
Additional options may be provided on the command line, and those options will
trump those specified in the options file.

The majority of options are paths to executables. A list of options, including
descriptions and default values are available by providing the --help option::

  $ python StepStone.py --help