Analysis Server
================

	
Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:

Introduction
-------------
The iFAB Foundry server encompasses all analysis tools developed by the DARPA iFAB Foundry project.  The server must be running prior to submitting designs for analysis.

System Requirements
---------------------

* Windows Server 2008 or Windows 7 (64-bit versions required)
* Avoid running on a virtual machine (VM)
* CPU: Recommend multi-core CPU, 2.5+ GHz
* RAM: Minimum 4 GBs, Recommend 2+ GBs per core
* Hard Disk: Minimum 20 GBs free, Recommend 100+ GBs free

Setup Instructions
--------------------

.. note::
    When asked to change environment variables, you may be required to restart your computer for the changes to have an affect.

#. Install pre-requisite software

   * Setup Java 7

        * Download the latest Java JDK 7 64-bit version for your computer architecture.
        * Follow the installation instructions provided by the installer. We will refer to the Java installation folder as <java_dir>
        * Add the JAVA_HOME environment variable with value <java_dir>
        * Add %JAVA_HOME%/bin to the PATH environment variable

   * Setup Ant

        * Download the latest Ant binary for your computer architecture from http://ant.apache.org/
        * Follow the installation instructions provided by the installer.  We will refer to the Ant installation folder as <ant_dir>
        * If not added by the installer, add <ant_dir>/bin to the PATH environment variable

   * Install Microsoft Visual Studio Redistributables

        * VC++ 2008 SP1 x86 from http://www.microsoft.com/en-us/download/details.aspx?id=5582
        * VC++ 2008 SP1 x64 from http://www.microsoft.com/en-us/download/details.aspx?id=2092
        * VC++ 2010 SP1 x86 from http://www.microsoft.com/en-us/download/details.aspx?id=8328
        * VC++ 2010 SP1 x64 from http://www.microsoft.com/en-us/download/details.aspx?id=1352
        * VC++ 2012 x86 from http://www.microsoft.com/en-us/download/details.aspx?id=30679
        * VC++ 2012 x64 from http://www.microsoft.com/en-us/download/details.aspx?id=30679

   * Install the 2012 R2 version of aPriori to :file:`C:\\aPriori2012R2` by following the associated installation documentation provided by aPriori.  Follow the instructions for installing the Demmer and Rock Island Automation VPEs.  This step is optional if aPriori is not available.  iFAB Foundry will still function without aPriori, but may produce different results than the official iFAB Foundry system

#. Extract this ZIP to a folder of your choice.  We will refer to this folder as <install_dir>
#. Setup Windows Environment Variables

     * Add <install_dir>/lib/oce-0.11/Win64/bin to PATH
     * Add <install_dir>/main/All/lib/win64 to PATH
     * Add <install_dir>/lib/atsv-5.1.8/jnilib to PATH
     * Add MMGT_OPT with value 0

#. Disable error popups on Windows.  This step is optional; however, you will need to manually close any popup messages.

     * Run the registry editor, regedit.exe
     * Set HKEY_LOCAL_MACHINE\\SYSTEM\\CurrentControlSet\\Control\\Windows\\ErrorMode to 2
     * Set HKEY_CURRENT_USER\\Software\\Microsoft\\Windows\\Windows Error Reporting\\DontShowUI to 1

Building and Running iFAB Foundry
-----------------------------------

The iFAB Foundry server encompasses all analysis tools developed by the DARPA iFAB Foundry project.  The server must be running prior to submitting designs for analysis.  Follow the steps below to compile and run the server.  Refer to the troubleshooting section at the end of this document if you encounter any issues.

#. When building iFAB Foundry, the resulting build is saved to :file:`C:\\iFoundry`

#. Navigate to the <install_dir>/main/All directory and run the command::

    ant deploy

#. Navigate to :file:`C:\\iFoundry\dist` and run the startup.bat script. This starts a local instance of iFAB Foundry
#. Next, run launchGUI.bat.  This opens the GUI for submitting designs and viewing the results
#. Login with username "test" and password "secret"
#. Using the GUI, you can submit designs for analysis and view the results.  Tip: double-click any row on the results page to view details
#. To shutdown, close the GUI and run shutdown.bat

Verifying your Installation
-----------------------------

Submit each of the designs in <install_dir>\\models\\EXAMPLES. The armor, machining, and assembly examles will always return successful. The casting example will only work if aPriori is available. Please refer to the Troubleshooting section if you encounter any issues.

Building the TDP Editor
-------------------------
The TDP Editor is a graphical tool for annotating STEP models with manufacturing information.  It (1) annotates individual parts with the manufacturing process used to manufacture the part (i.e., purchased, machining, casting, plate); and (2) annotates the assembly with join information (i.e., mechanical fasteners, welds, adhesive). The output from the TDP Editor is then submitted to the iFAB Foundry server for analysis.

#. The TDP Editor is built by running the command::
       
	   ant deploy-tdp-editor

#. To run the TDP Editor, navigate to the :file:`C:\\iFoundry\\dist\\TDPEditor` folder and run the TDPEditor.bat script

VehicleForge Submissions
--------------------------

Designs created using the AVM META/CyPhy tools (another component of the DARPA Adaptive Vehicle Make program) are submitted to iFAB Foundry for analysis using a command-line tool.  This command-line tool is embodied in the iFABFoundry-VehicleForgeInterface.jar library.  The use of this tool is best demonstrated by the Python scripts used by the META/CyPhy tools to bundle and submit designs.  These scripts can be found in the folder: :file:`<install_dir>/tools/TestBenches/DetailedManufacturingTestbench`

Troubleshooting
-----------------
If you experience issues when compiling or running this software, please refer to the following troubleshooting steps to resolve your issue.  If you are still unable to resolve this issue, please create a problem report on vehicleforge.org.

* If you receive the message "<command> is not recognized as an internal or external command, operable program or batch file", please ensure all the software outlined in the Setup     Instructions has been installed, that the environment variables have been correctly set, and restart your computer.  It may be necessary to add the directory containing the <command>     executable to your PATH environment variable.

* Make sure that the versions of the software installed in section Setup Instructions appear first on your PATH environment variable.  If not, there may be a conflict when more than one     version of the software is installed.
* If you attempt to verify the installation and you receive an error message or the design fails, check the logs in :file:`C:\\iFoundry\\dist\\logs` to see if there are any error messages.

* There are known issues when trying to compile and run this software on a virtual machine (VM).  If you are running a VM and experience file permission or other errors, please try     compiling and running this software on a system with Windows natively installed.

* Sometimes shutdown.bat won't close all of the windows.  If this happens, just manually close the windows.  You should always ensure the three Java windows (TaskManager, Worker, and     Tomcat) are closed.
* If the website at https://127.0.0.1:8443/ifab/ does not appear or indicates the server was not found, ensure that the following windows were created:

  * PostgreSQL
  * TaskManager
  * Worker (typically 4 workers are created) 
  * Tomcat

  If any of these windows are missing, check the following troubleshooting steps to resolve the issue.

* If the PostgreSQL window is missing, verify that there is no other PostgreSQL instance running.  First, ensure that no PostgreSQL service is running.  Second, open the task manager and check if postgres.exe is a running task. If so, kill this process.  Close the other windows and re-run startup.bat.
* If the TaskManager or Worker window is missing, first try closing all the windows and re-running startup.bat.  If this issue persists, edit startup.bat and increase the timeouts     used.  I.e., find the lines::

    timeout 10

  and change them to::

    timeout 20

  Close all windows and re-run startup.bat.  If this problem persists, look in the logs at :file:`C:\\iFoundry\\dist\\logs` to find any error messages.

* If the Tomcat window is missing, check the logs in
     :file:`C:\\iFoundry\\dist\\logs` and :file:`C:\\iFoundry\\dist\\apache-tomcat-7.0.21\\logs` for any error messages.
* If you receive an OutOfMemoryException from Java, try increasing the available memory by changing the -Xmx argument.  For example::

    java -Xmx8g
  
  allocates 8 GBs of memory.

* If there were any errors during the installation process or if you are making significant source code changes, it is helpful to start with a new, clean build.  This is accomplished with  the command::

    ant deploy-clean
  
  THIS COMMAND WILL DELETE THE :file:`C:\\iFoundry` FOLDER. THE IFAB FOUNDRY DATABASE AND ASSOCIATED FILES WILL BE DELETED.

* The iFAB Foundry system relies on system services, networking, databases, and the file system for its operation.  It is possible that certain Windows configurations and security software may inadvertently cause unexpected behavior.  If you continue to experience issues not resolved by these troubleshooting tips, we recommend *temporarily* disabling any security software and running the iFAB Foundry software with administrative permissions.  TAKE APPROPRIATE STEPS TO SAFEGUARD YOUR SYSTEM BEFORE DISABLING SECURITY SOFTWARE (E.G., DISCONNECT FROM NETWORK). If these actions resolve the issue, you will need to identify the configuration setting(s) affecting the software and make the necessary modifications.