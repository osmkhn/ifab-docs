Welcome to the iFab Manufacturing Tools documentation!
========================================================

.. note::
    This documentation is a work in progress. Topics marked with a TODO stub-icon are placeholders that have not been written yet. You can track the status of these topics through our public documentation `issue tracker <https://bitbucket.org/osmkhn/ifab-docs/issues?status=new&status=open>`_.


Introduction
-------------
	
The manufacture of complex cyber-physical systems is generally limited to two options: large scale manufacturing that is efficient at producing one known product quickly and repeatably, or prototype assembly of a single (or few) test article(s).  Factories that do one generally cannot efficiently do the other – it is currently not possible to achieve efficient large-scale manufacturing that is also flexible.  iFAB (instant Foundry, Adaptive through Bits) is an attempt to accomplish both in one manufacturing entity.  It aims to take a META-articulated system design representation and automatically configure a digitally programmable manufacturing facility tailored to fabricate it.  The IT infrastructure required to accomplish this is also able to provide detailed manufacturability information in the form of cost, schedule and identification of critical components to the designer throughout the design process.  Thus, the final verified META-articulated design sent for iFAB manufacturing already has manufacturing  considerations factored in.  The physical instantiation of the iFAB manufacturing capability, called a foundry, includes a network of participating manufacturing facilities and equipment, the sequencing of the product flow and production steps, as well as work instructions and training modules.  In essence, iFAB seeks to eliminate the learning curve inherent in large-scale manufacturing even for limited build numbers. The iFAB Foundry system is centered around two functions:

#. Providing manufacturability feedback to the designer at a level of detail commensurate to the design; and
#. Configuring a foundry of networked manufacturing capabilities tailored to the final verified design, including supply chain considerations, assembly planning, and automatically generated  computer-numerically-controlled (CNC) and human work instructions.

iFAB Foundry is a collection of open-source software tools developed by the Pennsylvania State University's Applied Research Laboratory.  These tools include:

* Analysis Server - The manufacturability analysis engine for rapidly assessing whether a given design is manufacturable and estimating a cost and lead time.
* Web Portal - Website for managing and viewing the status of jobs submitted to the analysis server.  Also contains the API for receiving and processing submissions from MAAT.
* Hull Design Assist Tool (HuDAT) - A CREO plugin for building the armor and internal structure for a ground vehicle.
* Manufacturing Analysis Augmentation Tool (MAAT) - A CREO plugin or standalone Java executable for augmenting a CAD model with manufacturing information, such as material, coatings, and tolerances.
* NC Code Generation - A MasterCAM plugin for automatically generating NC code for a given CAD model.


Notes
-------
The official iFAB Foundry system is managed and hosted by The Pennsylvania State University Applied Research Laboratory and DARPA. Due to the use of several commercial products including:

* aPriori (http://www.apriori.com/)
* HOOPS Exchange and HOOPS Publish (http://www.techsoft3d.com/)
* ACIS (http://www.spatial.com/)
* MasterCAM (http://www.mastercam.com/)

Components requiring these commercial products have been removed from this installation. This public release version will still function, but its results may differ from the official version.

Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:
		
    analysis-server/index
    web-portal/index
    hudat/index
    maat/index
    ncgen/index

Contribute
-----------

The documentation on this site is auto generated from `this <bitbucket.org/osmkhn/ifab-docs>`_ repository on BitBucket. You can contribute by submitting pull requests to contribute content or post corrections on the issue tracker. This is a volunteer effort to collect all information related to the iFab project in one place, be it README files, journal papers or project presentations publicaly released over time. This not an official documentation release by the DMDII. 