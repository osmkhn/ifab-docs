Web Portal
============

	
Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:

Introduction
-------------
The iFAB Foundry Web Portal is a website for monitoring the status of the
iFAB Foundry analysis server, viewing submitted jobs and their results,
and performing various administrative tasks.  The Web Portal also serves
as the endpoint for receiving designs submitted from the Manufacturing
Augmentation Analysis Tool.

Requirements
------------

#. A Linux system (Tested openSuse, SLES 11 SP3, Cent OS 6.5)
#. Ruby Version Manager (RVM)
#. Ruby (Tested 1.9.3, 2.0.*, 2.1.*)
#. Postgres Development Package (e.g., libpq-dev)


Installation Instructions
-------------------------

.. note::
    Installation instructions may vary from system to system. All commands run as a non privileged user that has access to sudo privileges.


#. Install RVM - Visit http://rvm.io and follow install instructions
#. Install Ruby

   * Run "rvm install ruby"
   * Install will prompt for sudo password to install ruby dependencies
 
#. Install Bundler - Run "gem install bundler"
#. Install the Postgres Development package (e.g., libpq-dev)
#. Install Gems - Run "bundle install" from the top level project folder
#. Setup the Database

   * Start the iFAB Foundry Analysis Server PostgreSQL database server
   * Create a new database user and database
   * Edit config/database.yml and update the host, database, username, and password fields
   * Run "bundle exec rake db:migrate"
#. Create a new secret token in config/initializers/secret_token.rb
#. Launch the web server - Run "bundle exec rails s"
