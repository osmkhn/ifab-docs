Hull Design Assist Tool (HuDAT)
================================

	
Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:

Introduction
-------------
The Hull Design Assist Tool (HuDAT) is a Creo plugin that enables three capabilities for designing and building a armored vehicle's hull:

#. Generate conceptual plates from a soapbar (a simple representation of the outline of the hull)
#. Generate detailed plates that include ballistic weld edge preparation for steel and aluminum armors
#. Weld internal structure to the plates to provide structural reinforcement

Requirements
------------

#. Creo C development toolkit
#. Boost 1_49
#. Visual Studio 2010 or later

Installation
------------

#. Open the Visual Studio solution (.sln) file in the :file:`src/DAT` directory
#. Point Visual Studio to the Boost include files
#. Compile the project to generate the DLLs
#. Open the Visual Studio solution (.sln) file in the :file:`src/DATInstaller` directory
#. Compile the project to generate the installer
#. Run the generated installer to install the HuDAT plugin