Manufacturing Analysis Augmentation Tool (MAAT)
================================================

	
Topics
-------

.. toctree::
    :maxdepth: 2
    :titlesonly:

Introduction
-------------
The Manufacturing Analysis Augmentation Tool (MAAT) is a Creo plugin that allows a user to annotate a Creo model (individual parts and assemblies) with manufacturing information.  This information includes the material, manufacturing process (machining, casting, etc.), general tolerances, and assembly information (weld, bolt, glue, etc.).  This tool can be used standalone within Creo or as part of the DARPA AVM toolchain.

Requirements
------------

#. Creo 2.0 with JLink
#. Java JDK 7
#. Apache Ant
#. Visual Studio 2010 or later (for the installer)


Setup
-----

#. Copy the file pfc.jar from JLink to the lib/ directory.  This is PTC Creo's Java interface library.  It is typically located here :file:`C:/Program Files (x86)/PTC/Creo 2.0/Common Files/M060/text/java/pfc.jar`

#. Copy the file JLinkDll.dll to the distlib/ directory.
#. Run "ant" in the root directory.  This compiles the MAAT plugin.
#. Run build/run.bat to launch Creo with the MAAT plugin.

Installer
---------

#. First follow the setup instructions above to compile the MAAT code
#. Open :file:`installer/CreoPluginInstaller/CreoPluginInstaller.sln` in Visual Studio
#. Build the solution
#. The installer will typically be saved to :file:`installer/CreoPluginInstaller/CreoPluginInstaller/Debug/CreoPluginInstaller.msi`

Notes
-----

The ability to submit designs directly to the iFAB Foundry servers is disabled. Contact the DARPA AVM program representatives or Penn State ARL for information to re-enable this feature.  Designs can still be submitted through GME/CyPhy with a valid VehicleForge account.